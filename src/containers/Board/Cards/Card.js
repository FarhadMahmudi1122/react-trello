import React, {PropTypes} from 'react';
import styles from '../../../components/CardStyles.js';
import Modal from 'react-modal';
import EditableLabel from '../../../components/EditableLabel';

import {connect} from 'react-redux';

class Card extends React.Component {
  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    item: PropTypes.object.isRequired,
    style: PropTypes.object,
    setCardMedium: PropTypes.func
  };

  constructor(props) {
    super(props);
    const { item } = this.props;
    this.state = {
      buttonDisplay: 'flex',
      isOpen: false,
      radioSelected: item.FullContent.ContentMedium.Type === 'image/jpg' ? 'image' : 'video',
      modalImageDisable: false,
      modalVideoDisable: true
    };
  }

  clicked() {
    this.setState({
      buttonDisplay: 'none'
    });
  }

  blured() {
    this.setState({
      buttonDisplay: 'flex'
    });
  }

  showModal() {
    this.setState({
      isOpen: true,
    });
  }

  hiddenModal() {
    this.setState({
      isOpen: false,
    });
  }


  render() {
    const { style, item } = this.props;
    const {
      buttonDisplay,
      isOpen,
      radioSelected,
      modalImageDisable,
      modalVideoDisable
    } = this.state;

    return (
      <div style={style} className="item" id={style ? item.id : null}>
        <div
          className="item-color-section"
          style={{
            backgroundColor: item.SubjectCategory.Color,
          }}
        >
        </div>
        <div className="item-header">
          <EditableLabel
            rows="2"
            cols="12"
            onChanged={(newValue) => {
              item.Header = newValue;
            }}
            value={item.Header}
          />
        </div>
        <div className="item-container-wrape">
          <div className="item-avatar-wrap">
            <input
              ref={(cardImageFileInput) => {
                this.cardImageFileInput = cardImageFileInput;
              }}
              type="file"
              onChange={() => {
                const { x, y } = this.props;
                const file = this.cardImageFileInput.files[0];
                const url = window.URL.createObjectURL(file);
                const type = 'image/jpg';
                const medium = {
                  File: file,
                  Url: url,
                  Type: type
                };
                console.log(x, y, medium);
                this.props.setCardMedium(x, y, 0, medium);
              }}
            />
            <img
              src={item.Media[0].Url}
              alt=""
              onClick={() => {
                this.cardImageFileInput.click();
              }}
            />
          </div>
          <div className="item-subHeader-desc">
            <EditableLabel
              style={{ height: '40px', width: '110px' }}
              rows="2"
              cols="12"
              onChanged={(newValue) => {
                item.Subheader = newValue;
              }}
              value={item.Subheader}
            />
          </div>
          <EditableLabel
            rows="4"
            cols="25"
            onChanged={(newValue) => {
              item.Description = newValue;
            }}
            value={item.Description}
          />
        </div>
        <div className="item-modal-and-buttons">
          <Modal
            ariaHideApp={false}
            isOpen={isOpen}
            contentLabel="Modal"
            shouldCloseOnEsc={false}
            shouldCloseOnOverlayClick={false}
          >
            <h3 style={styles.modalHeaderText}>محتوای خود را وارد کنید</h3>
            <div>
              <label style={{
                float: 'right',
                marginTop: 50,
                marginLeft: 20
              }}>
                <input
                  value="image"
                  type="radio"
                  name="videoOrImage"
                  checked={radioSelected === 'image'}
                  onChange={(e) => {
                    this.setState({
                      radioSelected: e.target.value,
                      modalVideoDisable: true,
                      modalImageDisable: false,
                    });
                  }}
                />
                انتخاب عکس
              </label>
              <div>
                <input
                  disabled={modalImageDisable}
                  ref={(modalImageFileInput) => {
                    this.modalImageFileInput = modalImageFileInput;
                  }}
                  type="file"
                  style={{ display: 'none' }}
                  onChange={() => {
                    const newModalSrc = window.URL.createObjectURL(this.modalImageFileInput.files[0]);
                    item.FullContent.ContentMedium.File = this.modalImageFileInput.files[0];
                    item.FullContent.ContentMedium.Url = newModalSrc;
                    item.FullContent.ContentMedium.Type = 'image/jpg';
                    this.setState({ newModalSrc });
                  }}
                />
                <img
                  style={styles.modalImageIMGtag}
                  src={item.FullContent.ContentMedium.Url}
                  alt=""
                  onClick={() => {
                    this.modalImageFileInput.click();
                  }}
                />
              </div>
            </div>
            <div>
              <label style={{
                float: 'right',
                marginTop: 50,
                marginLeft: 20,
                marginRight: 20
              }}>
                <input
                  value="video"
                  type="radio"
                  name="videoOrImage"
                  checked={radioSelected === 'video'}
                  onChange={(e) => {
                    this.setState({
                      radioSelected: e.target.value,
                      modalImageDisable: true,
                      modalVideoDisable: false
                    });
                  }}
                />
                انتخاب ویدئو
              </label>
              <EditableLabel
                onClick={modalVideoDisable}
                style={styles.modalVideoTextErea}
                rows="3"
                cols="12"
                onChanged={(newValue) => {
                  item.FullContent.ContentMedium.Url = newValue;
                  item.FullContent.ContentMedium.Type = 'video/mp4';
                  console.log('url is', newValue);
                }}
                value={item.FullContent.ContentMedium.Type === 'video/mp4' ?
                  item.FullContent.ContentMedium.Url :
                  ''}
              />
            </div>

            <EditableLabel
              style={styles.modalHeaderTextErea}
              rows="3"
              cols="12"
              onChanged={(newValueHeader) => {
                item.FullContent.Header = newValueHeader;
              }}
              value={item.FullContent.Header}
            />
            <EditableLabel
              style={styles.modalSubheaderTextErea}
              rows="3"
              cols="12"
              onChanged={(newValueSubheader) => {
                item.FullContent.Subheader = newValueSubheader;
              }}
              value={item.FullContent.Subheader}
            />
            <EditableLabel
              style={styles.modalTextErea}
              rows="8"
              cols="12"
              onChanged={(newValueText) => {
                item.FullContent.Text = newValueText;
              }}
              value={item.FullContent.Text}
            />
            <br/>
            <input
              form="form"
              onClick={() => {
                this.hiddenModal();
              }}
              className="btn btn-info btn btn-primary btn-sm"
              style={styles.modalButton}
              type="submit"
              value="تائید"
            />
          </Modal>

          <div className="color-and-confirm-buttons">
            <input
              ref={(colorinput) => {
                this.colorinput = colorinput;
              }}
              onChange={() => {
                var newColor = this.colorinput.value;
                item.SubjectCategory.Color = newColor;
                this.setState({ newColor })
              }}
              value={item.SubjectCategory.Color}
              type="color"
              id="colorPicker"
              style={styles.colorPicker}
              className="btn btn-info btn-sm"
            />
            <input
              onClick={() => {
                this.showModal();
              }}
              className="btn btn-info btn btn-primary btn-sm"
              type="submit"
              value="متن محتوا"
            />
          </div>
        </div>
      </div>
    );
  }
}

const modalStyles = {
  content: {
    top: '100px',
    left: '50%',
    right: '50%',
    bottom: '1%',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  }
};

export default connect(({ lists }) => ({ lists: lists.lists }))(Card);
