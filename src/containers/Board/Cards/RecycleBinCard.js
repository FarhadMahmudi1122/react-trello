import React, { PropTypes, Component } from 'react';
import { DragSource, DropTarget } from 'react-dnd';
import recycleBin from '../../../assets/images/Recycle-Bin-Full-icon.png';

import Card from './Card';

function getStyles(isDragging) {
  return {
    display: isDragging ? 0.5 : 1
  };
}

const specs = {
  drop(props, monitor) {
    const X = monitor.getItem().x;
    const Y = monitor.getItem().y;

    props.removeCard(X, Y);
  },
  hover() {
  }
};

@DropTarget('card', specs, (connectDragSource, monitor) => ({
  connectDropTarget: connectDragSource.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop(),
  item: monitor.getItem()
}))
export default class RecycleBinCard extends Component {
  render() {
    const { connectDropTarget, isDragging } = this.props;
    return connectDropTarget(
      <img
        className="control-panel-card-content" src={recycleBin}
        alt="شما می توانید کارتهای خود را اینجا حذف کنید."
      />
    );
  }
}

RecycleBinCard.propTypes = {
  connectDropTarget: PropTypes.func,
  isDragging: PropTypes.bool,
  isOver: PropTypes.bool,
  item: PropTypes.object,
  canDrop: PropTypes.bool,
};
