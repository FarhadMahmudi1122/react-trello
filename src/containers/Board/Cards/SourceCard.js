import React, {PropTypes, Component} from 'react';
import {DragSource} from 'react-dnd';

import Card from './Card';

function getStyles(isDragging) {
  return {
    display: isDragging ? 0.5 : 1
  };
}

const emptyCard = () => ({
  id: -1,
  Header: 'عنوان',
  Subheader: 'زیر عنوان',
  Description: 'متن',
  SubjectCategory: {
    Name: 'JAN',
    Color: '#D1D10F',
  },
  Media: [
    {
      Type: 'image/jpg',
      Url: null,
      File: undefined
    }
  ],
  FullContent: {
    Header: 'عنوان',
    Subheader: 'زیر عنوان',
    Text: 'متن محتوا',
    ContentMedium: {
      Type: 'image/jpg',
      Url: undefined,
      File: undefined
    }
  },
});

const cardSource = {
  beginDrag() {
    return emptyCard();
  }
};

function collectDragSource(connectDragSource, monitor) {
  return {
    connectDragSource: connectDragSource.dragSource(),
    connectDragPreview: connectDragSource.dragPreview(),
    isDragging: monitor.isDragging()
  };
}

@DragSource('source-card', cardSource, collectDragSource)
export default class SourceCard extends Component {
  render() {
    const { connectDragSource, isDragging } = this.props;
    return connectDragSource(
      <div>
        <Card
          style={getStyles(isDragging)}
          item={emptyCard()}
        />
      </div>
    );
  }
}

SourceCard.propTypes = {
  connectDragSource: PropTypes.func,
  isDragging: PropTypes.bool
};
