import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Modal from 'react-modal';
import Spinner from 'react-spinner';

import * as ListsActions from '../../actions/lists';

import EditableLabel from '../../components/EditableLabel';
import CardsContainer from './Cards/CardsContainer';
import CustomDragLayer from './CustomDragLayer';
import 'whatwg-fetch'
import SourceCard from './Cards/SourceCard';
import RecycleBinCard from './Cards/RecycleBinCard';
import detectDragModifierKeys from '../detectDragModifierKeys';

function mapStateToProps(state) {
  return {
    error: state.lists.error,
    isFetching: state.lists.isFetching,
    isUploading: state.lists.isUploading,
    lists: state.lists.lists,
    kaleske: state.lists.kaleske,
    currentWeek: state.lists.currentWeek
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ListsActions, dispatch);
}

@detectDragModifierKeys()
@connect(mapStateToProps, mapDispatchToProps)
@DragDropContext(HTML5Backend)
export default class Board extends Component {
  static propTypes = {
    initLists: PropTypes.func.isRequired,
    getLists: PropTypes.func.isRequired,
    uploadBoard: PropTypes.func.isRequired,
    moveCard: PropTypes.func.isRequired,
    copyCard: PropTypes.func,
    addCard: PropTypes.func.isRequired,
    setCurrentWeek: PropTypes.func.isRequired,
    setHeaderImage: PropTypes.func.isRequired,
    setSliderImage: PropTypes.func.isRequired,
    setSliderText: PropTypes.func.isRequired,
    setCardMedium: PropTypes.func.isRequired,
    removeCard: PropTypes.func.isRequired,
    moveList: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
    isUploading: PropTypes.bool.isRequired,
    error: PropTypes.bool.isRequired,
    lists: PropTypes.array.isRequired,
    kaleske: PropTypes.object.isRequired,
    currentWeek: PropTypes.number.isRequired,
    dragModifierKeys: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.moveCard = this.moveCard.bind(this);
    this.copyCard = this.copyCard.bind(this);
    this.addCard = this.addCard.bind(this);
    this.removeCard = this.removeCard.bind(this);
    this.moveList = this.moveList.bind(this);
    this.findList = this.findList.bind(this);
    this.scrollRight = this.scrollRight.bind(this);
    this.scrollLeft = this.scrollLeft.bind(this);
    this.stopScrolling = this.stopScrolling.bind(this);
    this.startScrolling = this.startScrolling.bind(this);
    this.setCurrentWeek = this.setCurrentWeek.bind(this);
    this.setHeaderImage = this.setHeaderImage.bind(this);
    this.setSliderImage = this.setSliderImage.bind(this);
    this.setSliderText = this.setSliderText.bind(this);
    this.setCardMedium = this.setCardMedium.bind(this);
    this.state = {
      isScrolling: false
    };
  }

  componentWillMount() {
    this.props.initLists();
  }

  startScrolling(direction) {
    // if (!this.state.isScrolling) {
    switch (direction) {
      case 'toLeft':
        this.setState({ isScrolling: true }, this.scrollLeft());
        break;
      case 'toRight':
        this.setState({ isScrolling: true }, this.scrollRight());
        break;
      default:
        break;
    }
    // }
  }

  scrollRight() {
    function scroll() {
      document.getElementsByTagName('main')[0].scrollLeft += 10;
    }

    this.scrollInterval = setInterval(scroll, 10);
  }

  scrollLeft() {
    function scroll() {
      document.getElementsByTagName('main')[0].scrollLeft -= 10;
    }

    this.scrollInterval = setInterval(scroll, 10);
  }

  stopScrolling() {
    this.setState({ isScrolling: false }, clearInterval(this.scrollInterval));
  }

  moveCard(lastX, lastY, nextX, nextY) {
    const { ctrlKey } = this.props.dragModifierKeys;
    if (ctrlKey) {
      this.copyCard(lastX, lastY, nextX, nextY);
    } else {
      this.props.moveCard(lastX, lastY, nextX, nextY);
    }
  }

  copyCard(lastX, lastY, nextX, nextY) {
    this.props.copyCard(lastX, lastY, nextX, nextY);
  }

  addCard(X, Y, item) {
    this.props.addCard(X, Y, item);
  }

  removeCard(X, Y) {
    this.props.removeCard(X, Y);
  }

  moveList(listId, nextX) {
    const { lastX } = this.findList(listId);
    this.props.moveList(lastX, nextX);
  }

  findList(id) {
    const { lists } = this.props;
    const list = lists.filter(l => l.id === id)[0];

    return {
      list,
      lastX: lists.indexOf(list)
    };
  }

  setCurrentWeek(weekNumber) {
    this.props.setCurrentWeek(weekNumber);
  }

  setHeaderImage(url, file) {
    this.props.setHeaderImage(url, file);
  }

  setSliderImage(url, file) {
    this.props.setSliderImage(url, file);
  }

  setSliderText(text) {
    this.props.setSliderText(text);
  }

  setCardMedium(x, y, i, medium) {
    this.props.setCardMedium(x, y, i, medium);
  }

  render() {
    const { lists, kaleske, currentWeek, setHeaderImage, setSliderImage, setSliderText } = this.props;

    console.log('rendering... ', this.props.isFetching, this.props.isUploading);
    return (
      <div style={{ height: '100%' }}>
        <Modal
          ariaHideApp={false}
          isOpen={this.props.isFetching || this.props.isUploading}
          style={{
            content: {
              position: 'absolute',
              left: 0,
              top: 0,
              width: '100%',
              height: '100%',
              backgroundColor: 'black',
              opacity: 0.8
            }
          }}
        >
          <Spinner/>
        </Modal>

        <div className="board-container">
          <CustomDragLayer snapToGrid={false}/>
          {lists.map((item, i) =>
            <CardsContainer
              key={item.id}
              id={item.id}
              item={item}
              moveCard={this.moveCard}
              addCard={this.addCard}
              removeCard={this.removeCard}
              setCardMedium={this.setCardMedium}
              moveList={this.moveList}
              startScrolling={this.startScrolling}
              stopScrolling={this.stopScrolling}
              isScrolling={this.state.isScrolling}
              x={i}
              dragModifierKeys={this.props.dragModifierKeys}
            />)}

        </div>
        <div className="board-control-panel">
          <div className="control-panel-card" style={{ overflowY: 'scroll' }}>
            <div className="desk-head">
              <div className="desk-name">کارت جدید</div>
            </div>
            <div className="desk-items">
              <SourceCard/>
            </div>
          </div>
          <div className="control-panel-card">
            <div className="desk-head">
              <div className="desk-name">حذف کارت</div>
            </div>
            <div className="desk-items">
              <RecycleBinCard
                removeCard={this.removeCard}
              />
            </div>
          </div>
          <div className="control-panel-card">
            <div className="desk-head">
              <div className="desk-name">متن کنار سایز بچه</div>
            </div>
            <div className="desk-items">
              <div className="control-panel-card-content">
                <EditableLabel
                  rows="7"
                  cols="4"
                  onChanged={(newValue) => {
                    setSliderText(newValue);
                  }}
                  value={kaleske.SliderText}
                />
              </div>
            </div>
          </div>

          <div className="control-panel-card">
            <div className="desk-head">
              <div className="desk-name">هدر اصلی</div>
            </div>
            <div className="desk-items">
              <div className="control-panel-card-content">
                <input
                  className="board-input"
                  name="HeaderImage"
                  ref={(fileInputHeaderImage) => {
                    this.fileInputHeaderImage = fileInputHeaderImage;
                  }}
                  type="file"
                  onChange={() => {
                    const file = this.fileInputHeaderImage.files[0];
                    const url = window.URL.createObjectURL(file);
                    setHeaderImage(url, file);
                  }}
                />
                <img
                  className="board-image"
                  src={kaleske.HeaderImage}
                  alt=""
                  onClick={() => {
                    this.fileInputHeaderImage.click();
                  }}
                />
              </div>
            </div>
          </div>

          <div className="control-panel-card">
            <div className="desk-head">
              <div className="desk-name">سایز بچه</div>
            </div>
            <div className="desk-items">
              <div className="control-panel-card-content">
                <input
                  className="board-input"
                  name="SliderImage"
                  ref={(fileInputSliderImage) => {
                    this.fileInputSliderImage = fileInputSliderImage;
                  }}
                  type="file"
                  onChange={() => {
                    const file = this.fileInputSliderImage.files[0];
                    const url = window.URL.createObjectURL(file);
                    setSliderImage(url, file);
                  }}
                />
                <img
                  className="board-image"
                  src={kaleske.SliderImage}
                  alt=""
                  onClick={() => {
                    this.fileInputSliderImage.click();
                  }}
                />
              </div>
            </div>
          </div>


          <div
            className="control-panel-card"
            style={{
              width: '28%',
              position: 'relative',
              right: '0',
              bottom: '0',
              marginRight: '0',
              marginLeft: '6px'
            }}
          >
            <label dir="rtl" className="board-week-number">
              شماره هفته:
              <select
                ref={(fileInputWeekNumber) => {
                  this.fileInputWeekNumber = fileInputWeekNumber;
                }}
                style={{ top: 0, width: '80%' }}
                value={this.props.currentWeek}
                onChange={() => {
                  const curWeek = parseInt(this.fileInputWeekNumber.value, 10);
                  this.setCurrentWeek(curWeek);
                }}
              >
                {[...Array(42).keys()].map((i) => (
                  <option key={i} value={i}>{i}</option>
                ))}
              </select>
            </label>
            {this.props.error &&
            (<label className="board-error-message" style={{ color: 'red', textWeight: 'bold' }}>
                خطا در ارسال یا دریافت اطلاعات
            </label>
            )}
            <button
              className="btn btn-info board-update-button"
              onClick={() => {
                console.log('onClick currentWeek:', currentWeek);
                this.props.getLists(currentWeek);
              }}
            >
              بروزرسانی اطلاعات از سرور
            </button>
            <button
              className="btn btn-info board-fetch-button"
              onClick={() => {
                this.props.uploadBoard(kaleske, lists, currentWeek);
              }}
            >آپلود به سرور
            </button>
          </div>
        </div>
      </div>
    );
  }
}


// @font-face
//   font-family IranSands
//   font-style normal
//   src url(Fonts iran_sans_mobile ttf)
// @font-face
//   font-family IranSands
//   src url(Fonts iran_sans_mobile_bold ttf)
//   font-weight bold
