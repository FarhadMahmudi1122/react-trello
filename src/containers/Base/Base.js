import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import styles from '../../components/CardStyles.js'

const propTypes = {
  children: PropTypes.element.isRequired
};


const BaseContainer = (props) => (
  <main>
    {props.children}
  </main>
);

BaseContainer.propTypes = propTypes;

export default connect((state) => (state))(BaseContainer);
