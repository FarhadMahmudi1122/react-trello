export default {
  modalHeaderText: {
    textAlign: 'right',

  },
  modalImageIMGtag: {
    textAlign: 'right',
    fontSize: 16,
    fontWeight: 'bold',
    float: 'right',
    right: 20,
    width: 100,
    height: 100,
    border: '1px solid #000'
  },
  modalVideoTextErea: {
    textAlign: 'right',
    fontSize: 16,
    fontWeight: 'bold',
    float: 'right',
    right: 20,
    width: '40%',
    height: '25px',
    marginTop: 30,
    marginRight: 30,
    border: '1px solid #000'

  },
  modalHeaderTextErea: {
    textAlign: 'right',
    fontSize: 16,
    fontWeight: 'bold',
    float: 'right',
    clear: 'both',
    right: 20,
    width: '80%',
    height: '10%'
  },
  modalSubheaderTextErea: {
    textAlign: 'right',
    fontSize: 16,
    fontWeight: 'bold',
    float: 'right',
    clear: 'both',
    right: 20,
    width: '80%',
    height: '10%'
  },
  modalTextErea: {
    textAlign: 'right',
    fontSize: 16,
    fontWeight: 'bold',
    float: 'right',
    clear: 'both',
    right: 20,
    width: '80%',
    height: '50%'
  },
  modalButton: {
    marginTop: 20,
    fontFamily: 'IranSans',
    clear: 'both',
    float: 'right',
    right: 20,
  },
  colorPicker: {
    marginRight: 90
  },

}
