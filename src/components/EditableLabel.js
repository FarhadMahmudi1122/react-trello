import React, {Component, PropTypes} from 'react';

export default class EditableLabel extends Component {
  static propTypes = {
    rows: PropTypes.string.isRequired,
    cols: PropTypes.string.isRequired,
    style: PropTypes.object,
    disabled: PropTypes.bool,
    value: PropTypes.string,
    onChanged: PropTypes.func.isRequired,
  };


  constructor(props) {
    super(props);

    this.labelClicked = this.labelClicked.bind(this);
    this.textChanged = this.textChanged.bind(this);
    this.inputLostFocus = this.inputLostFocus.bind(this);
    this.keyPressed = this.keyPressed.bind(this);

    this.state = {
      editing: false,
      text: this.props.value
    };
  }


  componentWillMount() {
    const { value } = this.props;

    if (value !== this.state.lastValue) {
      this.setState({ text: value, lastValue: value });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ text: nextProps.value });
    }
  }

  labelClicked() {
    this.setState({ editing: true });
  }

  textChanged() {
    this.setState({ text: this.textInput.value });
  }

  inputLostFocus() {
    this.setState({ editing: false });
    this.props.onChanged(this.state.text);
  }

  keyPressed(event) {
    if (event.key === 'Enter') {
      this.inputLostFocus();
    }
  }

  render() {
    const { onChanged, value } = this.props;

    if (this.state.editing) {
      return (
        <textarea
          dir="rtl"
          className="item-editableLabel-textarea board-slider-text"
          ref={(textInput) => {
            this.textInput = textInput;
          }}
          onChange={this.textChanged}
          onBlur={() => {
            this.inputLostFocus();
          }}
          onKeyPress={this.keyPressed}
          value={this.state.text}
          rows={this.props.rows}
          cols={this.props.cols}
          style={this.props.style}
          disabled={this.props.disabled}
          autoFocus
        />
      );
    }

    return (
      <div
        dir="rtl"
        onClick={() => {
          this.labelClicked();
        }}
        className="item-editableLabel-div board-slider-text"
        style={{ overflow: 'hidden', textAlign: 'right', ...this.props.style }}
      >
        {this.state.text}
      </div>
    );
  }
}
