import faker from 'faker';


export const INIT_LISTS = 'INIT_LISTS';
export const GET_LISTS_START = 'GET_LISTS_START';
export const GET_LISTS_ERROR = 'GET_LISTS_ERROR';
export const GET_LISTS = 'GET_LISTS';
export const UPLOAD_LISTS_START = 'UPLOAD_LISTS_START';
export const UPLOAD_LISTS_ERROR = 'UPLOAD_LISTS_ERROR';
export const UPLOAD_LISTS_FINISHED = 'UPLOAD_LISTS_FINISHED';
export const MOVE_CARD = 'MOVE_CARD';
export const COPY_CARD = 'COPY_CARD';
export const ADD_CARD = 'ADD_CARD';
export const REMOVE_CARD = 'REMOVE_CARD';
export const MOVE_LIST = 'MOVE_LIST';
export const TOGGLE_DRAGGING = 'TOGGLE_DRAGGING';
export const SET_CURRENT_WEEK = 'SET_CURRENT_WEEK';
export const SET_HEADER_IMAGE = 'SET_HEADER_IMAGE';
export const SET_SLIDER_IMAGE = 'SET_SLIDER_IMAGE';
export const SET_SLIDER_TEXT = 'SET_SLIDER_TEXT';
export const SET_CARD_MEDIUM = 'SET_CARD_MEDIUM';

const serverAddress = 'http://rafshadow.ir';

export function initLists() {
  const daysInWeek = 7;
  return (dispatch) => {
    const lists = [];
    let count = 0;
    for (let i = 0; i < daysInWeek; i++) {
      const cards = [];
      const randomQuantity = Math.floor(Math.random() * (9 - 1 + 1)) + 1;
      for (let ic = 0; ic < 0; ic++) {
        cards.push({
          id: count,
          Header: 'عنوان' + count,
          Subheader: 'زیر عنوان' + count,
          Description: 'متن',
          SubjectCategory: {
            Name: 'JAN',
            Color: '#D1D10F',
          },
          Media: [
            {
              Type: 'image/jpg',
              Url: null,
              File: undefined
            }
          ],
          FullContent: {
            Header: 'عنوان',
            Subheader: 'زیر عنوان',
            Text: 'متن محتوا',
            ContentMedium: {
              Type: 'image/jpg',
              Url: undefined,
              File: undefined
            }
          },
        });
        count = count + 1;
      }
      lists.push({
        id: i,
        name: `روز ${(i + 1)}`,
        cards
      });
    }
    const kaleske = {
      Product: 'Kaaleskeh',
      WeekNumber: 0,
      HeaderImage: null,
      HeaderImageFile: null,
      SliderImage: null,
      SliderImageFile: null,
      SliderText: 'کلیک کنید و سپس متن را وارد کنید.'
    };
    dispatch({ type: INIT_LISTS, lists, kaleske, largestId: count });
  };
}


async function getListsAsync(currentWeek, dispatch) {
  const url = `${serverAddress}/content-list/kaaleskeh/${currentWeek}/all`;
  let largestId = -1;

  const response = await (await fetch(url)).json();

  if (!response.Product) {
    dispatch({ type: GET_LISTS_ERROR });
    return;
  }

        const kaleske = Object.assign({}, response);
        delete kaleske.Id;

  const days = kaleske.Days;
  for (let iDay = 0; iDay < days.length; iDay++) {
    const day = kaleske.Days[iDay];
    const cards = day.Cards;
    for (let iCard = 0; iCard < cards.length; iCard++) {
      const card = day.Cards[iCard];
      if (card.FullContent.ContentMedium.Url && card.FullContent.ContentMedium.Type === 'image/jpg') {
        try {
          const blob = await (await fetch(`${serverAddress}${card.FullContent.ContentMedium.Url}`)).blob();
          card.FullContent.ContentMedium = {
            Url: window.URL.createObjectURL(blob),
            File: blob
          };
        } catch (e) {
          dispatch({ type: GET_LISTS_ERROR, payload: { iDay, iCard } });
        }
      } else {
        card.FullContent.ContentMedium.File = null;
      }

      const media = card.Media;
      for (let iMedium = 0; iMedium < card.Media.length; iMedium++) {
        const medium = media[iMedium];
        if (medium.Url) {
          const blob = await (await fetch(`${serverAddress}${medium.Url}`)).blob();
          Object.assign(medium, {
            File: blob,
            Url: `${serverAddress}${medium.Url}`
          });
        } else {
          Object.assign(medium, { File: null });
        }
      }

      Object.assign(card, { Media: media, id: ++largestId });
    }

    Object.assign(day, {
      id: iDay,
      cards,
      name: `روز ${(iDay + 1)}`
    });


    if (kaleske.HeaderImage) {
      const blob = await (await fetch(`${serverAddress}${kaleske.HeaderImage}`)).blob();
      Object.assign(kaleske, {
        HeaderImage: window.URL.createObjectURL(blob),
        HeaderImageFile: blob
      });
    }

    if (kaleske.SliderImage) {
      const blob = await (await fetch(`${serverAddress}${kaleske.SliderImage}`)).blob();
      Object.assign(kaleske, {
        SliderImage: window.URL.createObjectURL(blob),
        SliderImageFile: blob
      });
    }
  }
  dispatch(Object.assign({ type: GET_LISTS, largestId }, { lists: kaleske.Days, kaleske }));
}

export function getLists(currentWeek) {
  return dispatch => {
    dispatch({ type: GET_LISTS_START });
    Promise.resolve(getListsAsync(currentWeek, dispatch));
  };
}

export function uploadBoard(kaleske, lists, currentWeek) {
  return (dispatch) => {
    dispatch({ type: UPLOAD_LISTS_START });
    const json = Object.assign({}, kaleske, { Days: lists }, { WeekNumber: currentWeek });
    console.log('this is json', json);

    let appendNumber = 0;
    const formData = new FormData();
    formData.append('json', JSON.stringify(json));
    if (json.HeaderImageFile) {
      formData.append(`medium[${appendNumber}]`, json.HeaderImageFile, `file${appendNumber}.jpg`);
      appendNumber++;
    }
    if (json.SliderImageFile) {
      formData.append(`medium[${appendNumber}]`, json.SliderImageFile, `file${appendNumber}.jpg`);
      appendNumber++;
    }
    for (var i = 0; i < json.Days.length; i++) {
      for (var j = 0; j < json.Days[i].cards.length; j++) {
        if (json.Days[i].cards[j].FullContent.ContentMedium.File) {
          formData.append(`medium[${appendNumber}]`,
            json.Days[i].cards[j].FullContent.ContentMedium.File, `file${appendNumber}.jpg`);
          appendNumber++;
        }
        const mediaCount = json.Days[i].cards[j].Media.length;
        for (var k = 0; k < mediaCount; k++) {
          if (json.Days[i].cards[j].Media[k].File) {
            // console.log('media file', json.Days[i].cards[j].Media[k].File);
            formData.append(`medium[${appendNumber}]`,
              json.Days[i].cards[j].Media[k].File, `file${appendNumber}.jpg`);
            appendNumber++;
          }
        }
      }
    }

    fetch(`${serverAddress}/request`, {
      method: 'POST',
      config: {
        headers: {
          'Accept': 'application/json, */*',
          'content-type': 'multipart/form-data'
        },
        mode: 'no-cors'
      },
      body: formData
    })
      .then(response => response.text())
      .then(response => {
        dispatch({ type: UPLOAD_LISTS_FINISHED });
        console.log('response: ', response);
      })
      .catch(error => {
        dispatch({ type: UPLOAD_LISTS_ERROR });
        console.log('error: ', error);
      });
  };
}

export function moveList(lastX, nextX) {
  return (dispatch) => {
    dispatch({ type: MOVE_LIST, lastX, nextX });
  };
}

export function moveCard(lastX, lastY, nextX, nextY) {
  return (dispatch) => {
    dispatch({ type: MOVE_CARD, lastX, lastY, nextX, nextY });
  };
}

export function copyCard(lastX, lastY, nextX, nextY) {
  return (dispatch) => {
    dispatch({ type: COPY_CARD, lastX, lastY, nextX, nextY });
  };
}

export function addCard(X, Y, item) {
  return (dispatch) => {
    dispatch({ type: ADD_CARD, X, Y, item });
  };
}

export function removeCard(X, Y) {
  return (dispatch) => {
    dispatch({ type: REMOVE_CARD, X, Y });
  };
}

export function toggleDragging(isDragging) {
  return (dispatch) => {
    dispatch({ type: TOGGLE_DRAGGING, isDragging });
  };
}

export function setCurrentWeek(currentWeek) {
  return (dispatch) => {
    dispatch({ type: SET_CURRENT_WEEK, currentWeek });
  };
}

export function setHeaderImage(url, file) {
  return (dispatch) => {
    dispatch({ type: SET_HEADER_IMAGE, headerImage: { Url: url, File: file } });
  };
}

export function setSliderImage(url, file) {
  return (dispatch) => {
    dispatch({ type: SET_SLIDER_IMAGE, sliderImage: { Url: url, File: file } });
  };
}

export function setSliderText(text) {
  return (dispatch) => {
    dispatch({ type: SET_SLIDER_TEXT, text });
  };
}

export function setCardMedium(x, y, i, medium) {
  return (dispatch) => {
    dispatch({ type: SET_CARD_MEDIUM, x, y, i, medium });
  };
}
