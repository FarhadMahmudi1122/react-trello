/* eslint-disable global-require */
require('babel-polyfill');
/* eslint-enable global-require */
import React from 'react';
import { render } from 'react-dom';
import { Provider, connect } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import { routes } from './routes';

import configureStore from './store/configureStore';

import './assets/temp.styl';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

const RouterWithRedux = connect()(Router);

render(
  <Provider store={store}>
    <RouterWithRedux history={history} routes={routes} />
  </Provider>, document.getElementById('app')
);
