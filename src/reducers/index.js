import { combineReducers } from 'redux';
import {
  routerReducer
} from 'react-router-redux';

import lists from './lists';
import card from './card';


const rootReducer = combineReducers({
  routing: routerReducer,
  lists,
  card
});

export default rootReducer;
