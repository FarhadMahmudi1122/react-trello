import { Record } from 'immutable';

import {
  ADD_CARD,
  REMOVE_CARD,
  INIT_LISTS,
  GET_LISTS,
  GET_LISTS_START,
  MOVE_CARD,
  COPY_CARD,
  MOVE_LIST,
  TOGGLE_DRAGGING,
  SET_CURRENT_WEEK,
  SET_HEADER_IMAGE,
  SET_SLIDER_IMAGE,
  SET_CARD_MEDIUM, SET_SLIDER_TEXT, GET_LISTS_ERROR, UPLOAD_LISTS_START, UPLOAD_LISTS_ERROR, UPLOAD_LISTS_FINISHED,
} from '../actions/lists';

/* eslint-disable new-cap */
const InitialState = Record({
  isFetching: false,
  isUploading: false,
  error: false,
  lists: [],
  isDragging: false,
  kaleske: {},
  largestId: -1,
  currentWeek: 0
});
/* eslint-enable new-cap */
export const initialState = new InitialState;


export default function lists(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_WEEK:
      return state.withMutations((ctx) => {
        ctx.set('currentWeek', action.currentWeek);
      });
    case GET_LISTS_START:
      return state.withMutations(ctx => {
        ctx.set('isFetching', true)
          .set('error', false);
      });
    case GET_LISTS_ERROR: {
      return state.withMutations(ctx => {
        ctx.set('isFetching', false)
          .set('currentWeek', state.kaleske.WeekNumber)
          .set('error', true);
      });
    }
    case INIT_LISTS:
      return state.withMutations((ctx) => {
        ctx.set('lists', action.lists)
          .set('kaleske', action.kaleske)
          .set('largestId', action.largestId);
      });
    case GET_LISTS: {
      const newLists = [...action.lists];
      const newKaleske = { ...action.kaleske };
      return state.withMutations((ctx) => {
        ctx.set('isFetching', false)
          .set('error', false)
          .set('lists', newLists)
          .set('kaleske', newKaleske)
          .set('largestId', action.largestId);
      });
    }
    case UPLOAD_LISTS_START: {
      return state.withMutations(ctx => {
        ctx.set('isUploading', true)
          .set('error', false);
      });
    }
    case UPLOAD_LISTS_ERROR: {
      return state.withMutations(ctx => {
        ctx.set('isUploading', false)
          .set('error', true);
      });
    }
    case UPLOAD_LISTS_FINISHED: {
      return state.set('isUploading', false);
    }
    case MOVE_CARD: {
      const newLists = [...state.lists];
      const { lastX, lastY, nextX, nextY } = action;
      if (lastX === nextX) {
        newLists[lastX].cards.splice(nextY, 0, newLists[lastX].cards.splice(lastY, 1)[0]);
      } else {
        // move element to new place
        newLists[nextX].cards.splice(nextY, 0, newLists[lastX].cards[lastY]);
        // delete element from old place
        newLists[lastX].cards.splice(lastY, 1);
      }
      return state.withMutations((ctx) => {
        ctx.set('lists', newLists);
      });
    }
    case COPY_CARD: {
      const newLists = [...state.lists];
      const { lastX, lastY, nextX, nextY } = action;
      const newId = state.largestId + 1;
      const deepCopy = (o) => {
        const output = Array.isArray(o) ? [] : {};
        let key;
        let v;
        for (key in o) {
          v = o[key];
          output[key] = (typeof v === 'object') ? deepCopy(v) : v;
        }
        return output;
      };
      newLists[nextX].cards.splice(nextY, 0, Object.assign({}, deepCopy(newLists[lastX].cards[lastY]), { id: newId }));
      return state.withMutations((ctx) => {
        ctx.set('lists', newLists)
          .set('largestId', newId);
      });
    }
    case ADD_CARD: {
      const newLists = [...state.lists];
      const { X, Y, item } = action;
      const newId = state.largestId + 1;
      newLists[X].cards.splice(Y, 0, Object.assign({}, item, { id: newId }));
      return state.withMutations((ctx) => {
        ctx.set('lists', newLists)
          .set('largestId', newId);
      });
    }
    case REMOVE_CARD: {
      const newLists = [...state.lists];
      const { X, Y } = action;
      newLists[X].cards.splice(Y, 1);
      return state.withMutations((ctx) => {
        ctx.set('lists', newLists);
      });
    }
    case MOVE_LIST: {
      const newLists = [...state.lists];
      const { lastX, nextX } = action;
      const t = newLists.splice(lastX, 1)[0];

      newLists.splice(nextX, 0, t);

      return state.withMutations((ctx) => {
        ctx.set('lists', newLists);
      });
    }
    case SET_HEADER_IMAGE: {
      const newKaleske = Object.assign({}, state.kaleske);
      const { headerImage } = action;
      newKaleske.HeaderImage = headerImage.Url;
      newKaleske.HeaderImageFile = headerImage.File;
      return state.withMutations((ctx) => {
        ctx.set('kaleske', newKaleske);
      });
    }
    case SET_SLIDER_IMAGE: {
      const newKaleske = Object.assign({}, state.kaleske);
      const { sliderImage } = action;
      Object.assign(newKaleske, sliderImage);
      newKaleske.SliderImage = sliderImage.Url;
      newKaleske.SliderImageFile = sliderImage.File;
      return state.withMutations((ctx) => {
        ctx.set('kaleske', newKaleske);
      });
    }
    case SET_SLIDER_TEXT: {
      const newKaleske = Object.assign({}, state.kaleske);
      const { text } = action;
      Object.assign(newKaleske, { SliderText: text });
      return state.withMutations((ctx) => {
        ctx.set('kaleske', newKaleske);
      });
    }
    case SET_CARD_MEDIUM: {
      const newLists = [...state.lists];
      const { x, y, i, medium } = action;
      newLists[x].cards[y].Media.splice(i, 1, medium);
      return state.withMutations((ctx) => {
        ctx.set('lists', newLists);
      });
    }
    case TOGGLE_DRAGGING: {
      return state.set('isDragging', action.isDragging);
    }
    default:
      return state;
  }
}
